# Magrathea Trading Outpost


## Table of contents

- [Project Description](#project-description)
- [Project Features](#project-features)
- [Setup](#setup)
- [Bugs and feature requests](#bugs-and-feature-requests)
- [Technologies used](#technologies-used)
- [Creators](#creators)
- [Thanks](#thanks)
- [Copyright and license](#copyright-and-license)

## Project Description
Magrathea Trading Outpost is a tool used by finance managers to access their assigned clients’ portfolios and buy or sell stocks on their behalf. It is an application meant to be used internaly by the associates of a given compoany. Access to it is granted by an authorized admin.

### Project Features:

The application can be used in two modes: `Admin` or `Manager`.

Those modes correspond to the two roles that users can have in the system. When new admins and managers are created they are given login creadentials, with wich they can access the system. Accounts are also created for `clients`, but they have no access to the system and rely on managers to perform portfolio management on their behalf.

 - `Admins` have the access needed to create new accounts of all three types. They can also update those accounts, for instance they can change the personal data of the accounts or reassign a customer from one manager to another. Admins do not have access to the portfolio of clients or to finantial data about the finantial instruments or positions.

 - `Clients` have an account created for them, but they have no access to the system. The management of their portfolio and finances is done trough their assigned manager. Never the less a client can have:
   - watchlist
   - list of active position
   - hystory of closed positions

 - `Managers` can have clients assigned to them by an admin. Upon loggin in with a manager account they enter the
 application in `overview` mode and are able to view:
   - all customers, that are assigned to them
   - all available finantial instruments
   - a list of all the positions of their assigned customer, whether open or closed.

    When researching the stocks to offer to the customer the manager can view a `life chart` of the price of any given instrument. From the list of users the manager can open the portfolio of a customer in `edit mode`, which will give him the ability to edit the users profile by:
    - adding instruments to the clients watchlist
    - opening / closing positions

## Architecture

The architecture of the project tries to adhere to the guidelines for an angular-cli generated project. The logic of the app is separated in multiple modules.

Aside from the AppModule there are:
 - CoreModule holding services that are used troughout the application
 - SharedModule that holds components that are needed in multiple modules
 - AngularMaterialModule where components from AngularMaterial are held
 - RouterModule for the applications routing

Other feature modules that are loaded lazily are:
 - AdminModule holding the functionality needed for the admin users
 - ManagerModule holding the functionality needed for the manager users
 - PortfolioModule holding the functionality needed for editing the portfolio of a given client

## Technologies used

 - [Angular](https://angular.io/)
 - [Angular Material](https://material.angular.io/)
 - [NestJS](https://nestjs.com/)
 - [Node.js](https://nodejs.org/en/)
 - [Karma](https://karma-runner.github.io/latest/index.html)
 - [CanvasJS](https://canvasjs.com/), [ChartJS](https://www.chartjs.org/)
 - [MariaDB](https://mariadb.org/)
 - [TypeORM](http://typeorm.io/#/)
## Setup
### Prerequisites
* [Node.js](https://nodejs.org/)
* [MariaDB](https://downloads.mariadb.org/)

### Database
create a database schema using mysql
### Server
clone repository:
```sh
$ git clone https://gitlab.com/Telerik_Team_1/magrathea-server.git
```
run:
```sh
$ npm i
```
create a .env file with the database connection information as required in the config.service.ts

generate an initial migration:
```sh
$ npm run migration:generate -- --name=Initial
```
run the initial migration:
```sh
$ npm run migration:run
```
generate users seed data:
```sh
$ npm run setup
```
generate prices history data:
```sh
$ npm run update
```
start server with:
```sh
$ npm run start:dev
```
### Client
clone repository:
```sh
$ git clone https://gitlab.com/Telerik_Team_1/magrathea-trading-outpost.git
```
run:
```sh
$ npm i
```
run the server:
```sh
$ ng s -o
```
### Running tests
 Tests on the cliend are run with the command 'ng test'


## Creators

**[Stoyan Peshev](https://my.telerikacademy.com/Users/ogonommo)**  && **[Nikola Zlatinov](https://my.telerikacademy.com/Users/nizlatinov)**

## Thanks

Thanks to all contributors for their suport, including but not limited to:
 - The coleagues
 - The trainers
   - [Martin Veshev](https://github.com/vesheff)
   - [Rosen Urkov](https://github.com/RosenUrkov)
 - The mentor
   - [Alexander Targov](https://github.com/freeride8)
 - Everyone that feel they helped in any way

## Copyright and license

Code and documentation copyright 2018 the authors. Code released under the [MIT License](https://gitlab.com/Telerik_Team_1/magrathea-trading-outpost/blob/master/LICENSE).


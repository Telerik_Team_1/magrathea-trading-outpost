import { Component } from '@angular/core';

import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-complete-portfolio-dialog',
  templateUrl: './complete-portfolio-dialog.component.html',
  styleUrls: ['./complete-portfolio-dialog.component.css'],
})
export class CompletePortfolioDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<CompletePortfolioDialogComponent>,
) {}

  public cancel(): void {
    this.dialogRef.close(false);
  }

  public finish(): void {
    this.dialogRef.close(true);
  }

}

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Order } from 'src/app/models/order.model';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-client-history',
  templateUrl: './client-history.component.html',
  styleUrls: ['./client-history.component.css']
})
export class ClientHistoryComponent implements OnInit, OnDestroy {

  public orders: Order[] = [];

  public displayedColumns: string[] = [
    'company',
    'closedate',
    'buyprice',
    'sellprice',
    'profit',
  ];

  @ViewChild(MatSort) public sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public dataSource;

  public constructor(
    private readonly route: ActivatedRoute,
  ) {
  }

  public ngOnInit(): void {
    this.orders = this.route.snapshot.data['orders'];

    this.dataSource = new MatTableDataSource<Order>(this.orders);
    this.dataSource.sort = this.sort;
    setTimeout(() => { this.dataSource.paginator = this.paginator; });
  }

  public ngOnDestroy(): void {
    // destroy logic if any
  }

  public search(key: string): void {
    this.dataSource.filter = key.trim().toLowerCase();
  }

}

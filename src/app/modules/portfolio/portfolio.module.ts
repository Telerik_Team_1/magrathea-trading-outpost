import { NgModule } from '@angular/core';
import { PortfolioRoutingModule } from './portfolio-routing';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { SocketService } from '../manager/services/socket.service';
import { UpdaterService } from '../manager/services/updater.service';
import { PortfolioRoutesActivatorService } from './services/portfolio-routes-activator.service';
import { PortfolioRoutesDeactivatorService } from './services/portfolio-routes-deactivator.service';
import { ClientOrdersComponent } from './client-orders/client-orders.component';
import { ClientWatchlistComponent } from './client-watchlist/client-watchlist.component';
import { ClientWatchlistResolverService } from './services/client-watchlist-resolver';
import { AddCompanyDialogComponent } from './add-company-dialog/add-company-dialog.component';
import { BuyStockDialogComponent } from './buy-stock-dialog/buy-stock-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClientHistoryComponent } from './client-history/client-history.component';
import { ClientHistoryResolverService } from './services/client-history-resolver';
import { CompletePortfolioDialogComponent } from './complete-portfolio-dialog/complete-portfolio-dialog.component';
import { CompletePortfolioService } from './services/complete-portfolio.service';
import { ClientOverviewResolverService } from './services/client-overview-resolver';
import { ClientOverviewComponent } from './client-overview/client-overview.component';
import { UpdateFundsDialogComponent } from './update-funds-dialog/update-funds-dialog.component';


@NgModule({
  declarations: [
    ClientOrdersComponent,
    ClientWatchlistComponent,
    ClientHistoryComponent,
    AddCompanyDialogComponent,
    BuyStockDialogComponent,
    CompletePortfolioDialogComponent,
    ClientOverviewComponent,
    UpdateFundsDialogComponent,
  ],
  imports: [
    PortfolioRoutingModule,
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    SocketService,
    UpdaterService,
    ClientWatchlistResolverService,
    PortfolioRoutesActivatorService,
    PortfolioRoutesDeactivatorService,
    ClientHistoryResolverService,
    CompletePortfolioService,
    ClientOverviewResolverService,
  ],
  entryComponents: [
    AddCompanyDialogComponent,
    BuyStockDialogComponent,
    CompletePortfolioDialogComponent,
    UpdateFundsDialogComponent,
  ]
})
export class PortfolioModule {}

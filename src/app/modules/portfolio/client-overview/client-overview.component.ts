import { Component, OnInit, OnDestroy } from '@angular/core';
import { Client } from 'src/app/models/client.model';
import { ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { SocketService } from '../../manager/services/socket.service';
import { UpdaterService } from '../../manager/services/updater.service';
import { FormBuilder, FormGroup, AbstractControl, FormControl, Validators } from '@angular/forms';
import { UserShortOverview } from 'src/app/models/user-short-overview';
import { API_URL } from 'src/app/common/urls';
import { ErrorText } from 'src/app/common/error-texts';
import { MatDialog } from '@angular/material';
import { UpdateFundsDialogComponent } from '../update-funds-dialog/update-funds-dialog.component';
import { RequestService } from '../../core/request.service';
import { NotifierService } from '../../core/notifier.service';
import { PortfolioService } from '../../core/portfolio.service';

@Component({
  selector: 'app-client-overview',
  templateUrl: './client-overview.component.html',
  styleUrls: ['./client-overview.component.css']
})
export class ClientOverviewComponent implements OnInit, OnDestroy {

  public userId: string;
  public user;
  public id: string;
  public showForm = true;
  public maxDOB = new Date();
  public editUserForm: FormGroup;
  public name: AbstractControl;
  public dob: AbstractControl;
  public address: AbstractControl;
  public email: AbstractControl;
  public password: AbstractControl;
  public manager = new FormControl();
  public options: UserShortOverview[] = [];

  public client;
  private clientSubscribtion: Subscription;
  public clients: Client[] = [];

  public constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly socketService: SocketService,
    private readonly updaterService: UpdaterService,
    private readonly formBuilder: FormBuilder,
    private readonly requester: RequestService,
    private readonly notifier: NotifierService,
    private readonly portfolioService: PortfolioService,
    public dialog: MatDialog,
  ) {
    this.user = this.activatedRoute.snapshot.data['user'];
    this.id = this.portfolioService.id;
    this.name = this.formBuilder.control('', [Validators.required]);
    this.email = this.formBuilder.control('', [Validators.required, Validators.email]);
    this.dob = this.formBuilder.control('', [Validators.required]);
    this.address = this.formBuilder.control('', [Validators.required]);
    if ( this.user.role.rolename !== 'client') {
      this.password = this.formBuilder.control('');
    } else {
      this.manager = this.formBuilder.control('');
    }

    this.editUserForm = this.formBuilder.group({
      name: this.name,
      dob: this.dob,
      address: this.address,
      email: this.email,
      password: this.password,
      manager: this.manager,
    });
  }

  public ngOnInit() {
    this.maxDOB.setFullYear(this.maxDOB.getFullYear() - 18);
    this.setOriginalFormValue();

    this.socketService.mount();
    this.clientSubscribtion = this.socketService
      .clients()
      .subscribe((clients: Client[]) => {
        this.updaterService.updateclients(this.clients, clients);
        this.client = this.clients.filter(client => client.id === this.id)[0];
        });
  }

  public createForm() {

    this.name = this.formBuilder.control('', [Validators.required]);
    this.email = this.formBuilder.control('', [Validators.required, Validators.email]);
    this.dob = this.formBuilder.control('', [Validators.required]);
    this.address = this.formBuilder.control('', [Validators.required]);
    if ( this.user.role.rolename !== 'client') {
      this.password = this.formBuilder.control('');
    } else {
      this.manager = this.formBuilder.control('');
    }

    this.editUserForm = this.formBuilder.group({
      name: this.name,
      dob: this.dob,
      address: this.address,
      email: this.email,
      password: this.password,
      manager: this.manager,
    });
  }

  public displayFn(manager?): string | undefined {
    return manager ? `${manager.name} (${manager.email})` : undefined;
  }

  private _filter(name: string): UserShortOverview[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }

  private updateUser(): void {
    const id = this.portfolioService.id;
    this.password.setValue('');

    this.requester.post(API_URL('users', id), this.editUserForm.value)
    .subscribe(
      (response) => {
      this.notifier.success(`${this.name.value}'s profile was udpated!`);
    },
    error => {
      this.notifier.error('Updating the user failed!', error.error.message);
    });
  }

  private setOriginalFormValue() {
    this.showForm = false;
    setTimeout(() => {
      this.createForm();
      this.editUserForm.patchValue({
        name: this.user.fullname,
        email: this.user.email,
        address: this.user.address,
        dob: this.user.dob.split('T')[0],
      });
        this.showForm = true;
        this.manager.setValue(this.user.manager);
      });
  }

  public validate(control: AbstractControl): string {
    return control.hasError('required') ? ErrorText.requiredField : '';
  }

  public deposit(): void {
    this.onDialog(true);
  }

  public withdraw(): void {
    this.onDialog(false);
  }

  private onDialog(add: boolean): void {
    const dialogRef = this.dialog.open(UpdateFundsDialogComponent, {
      width: '350px',
      data: {
        client: this.client,
        add,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      // do
    });
  }

  public ngOnDestroy() {
    this.socketService.unmount();
    this.clientSubscribtion.unsubscribe();
  }
}

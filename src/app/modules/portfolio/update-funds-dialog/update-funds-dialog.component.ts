import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Client } from 'src/app/models/client.model';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { API_URL } from 'src/app/common/urls';
import { NotifierService } from '../../core/notifier.service';
import { RequestService } from '../../core/request.service';

@Component({
  selector: 'app-update-funds-dialog',
  templateUrl: 'update-funds-dialog.component.html',
  styleUrls: ['./update-funds-dialog.component.css'],
})
export class UpdateFundsDialogComponent implements OnInit {

  public client: Client;
  public add: boolean;
  public buttonText: string;

  public updateFundsForm: FormGroup;
  public amount: AbstractControl;

  private ticker;

  public constructor(
    private readonly formBuilder: FormBuilder,
    private readonly notifier: NotifierService,
    public dialogRef: MatDialogRef<UpdateFundsDialogComponent>,
    private readonly requester: RequestService,
    @Inject(MAT_DIALOG_DATA) public data: {
      client: Client,
      add: boolean,
    }
  ) {
    this.client = data.client;
    this.add = data.add;
    this.buttonText = this.add ? 'Deposit' : 'Withdraw';
  }

  public ngOnInit(): void {
    this.amount = this.formBuilder.control('0', [Validators.required]);
    this.updateFundsForm = this.formBuilder.group({
      amount: this.amount,
    });
    // this.ticker
  }

  public update(): void {
    if (this.amount.value < 0) {
      this.notifier.warning(`You can't buy less than 0.`);

      return;
    }

    if (!this.add && this.amount.value > this.client.funds) {
      this.notifier.warning(`Insufficient funds.`);

      return;
    }
    this.requester.put(API_URL('users', 'funds'), {
        id: this.client.id,
        add: this.add,
        amount: this.amount.value,
    }).subscribe(
      (result) => this.dialogRef.close(),
      (error: Error) => { this.notifier.error(error.message); }
    );
  }

  public validateUnits(amount: number): string {
    if (amount < 0) {
      return `Please provide a positive number.`;
    }

    return !this.add && amount > this.client.funds ? `You don't have sufficient funds` : '';
  }

  public onNoClick(): void {
    this.dialogRef.close();
  }

}

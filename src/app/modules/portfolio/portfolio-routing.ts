import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ClientOverviewComponent } from './client-overview/client-overview.component';
import { PortfolioRoutesActivatorService } from './services/portfolio-routes-activator.service';
import { PortfolioRoutesDeactivatorService } from './services/portfolio-routes-deactivator.service';
import { ClientOrdersComponent } from './client-orders/client-orders.component';
import { ClientWatchlistComponent } from './client-watchlist/client-watchlist.component';
import { ClientWatchlistResolverService } from './services/client-watchlist-resolver';
import { ClientHistoryResolverService } from './services/client-history-resolver';
import { ClientHistoryComponent } from './client-history/client-history.component';
import { ClientOverviewResolverService } from './services/client-overview-resolver';
import { ManagerRoutesActivatorService } from '../core/route-guards/manager-routes-activator.service';
import { AuthRouteActivatorService } from '../core/route-guards/auth-route-activator.service';

const routes: Routes = [
  {
    path: '',
    component: ClientOverviewComponent,
    canActivate: [ManagerRoutesActivatorService, AuthRouteActivatorService, PortfolioRoutesActivatorService],
    canDeactivate: [PortfolioRoutesDeactivatorService],
    resolve: { user: ClientOverviewResolverService },
  },
  {
    path: 'orders',
    component: ClientOrdersComponent,
    canActivate: [ManagerRoutesActivatorService, AuthRouteActivatorService, PortfolioRoutesActivatorService],
    canDeactivate: [PortfolioRoutesDeactivatorService],
  },
  {
    path: 'watchlist',
    component: ClientWatchlistComponent,
    canActivate: [ManagerRoutesActivatorService, AuthRouteActivatorService, PortfolioRoutesActivatorService],
    canDeactivate: [PortfolioRoutesDeactivatorService],
    resolve: { companies: ClientWatchlistResolverService },
  },
  {
    path: 'history',
    component: ClientHistoryComponent,
    canActivate: [ManagerRoutesActivatorService, AuthRouteActivatorService, PortfolioRoutesActivatorService],
    canDeactivate: [PortfolioRoutesDeactivatorService],
    resolve: { orders: ClientHistoryResolverService },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortfolioRoutingModule {}

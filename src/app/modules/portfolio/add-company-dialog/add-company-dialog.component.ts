import { Component, Inject, ViewChild } from '@angular/core';

import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSort,
  MatPaginator,
  MatTableDataSource,
} from '@angular/material';
import { Company } from 'src/app/models/company.model';
import { API_URL } from 'src/app/common/urls';
import { RequestService } from '../../core/request.service';
import { NotifierService } from '../../core/notifier.service';

@Component({
  selector: 'app-add-company-dialog',
  templateUrl: 'add-company-dialog.component.html',
})
export class AddCompanyDialogComponent {
  public companies: Company[] = [];

  private dataSource;
  @ViewChild(MatSort) public sort: MatSort;
  @ViewChild(MatPaginator) public paginator: MatPaginator;

  public displayedColumns = ['id', 'name', 'sell', 'add'];

  constructor(
    public dialogRef: MatDialogRef<AddCompanyDialogComponent>,
    private readonly requester: RequestService,
    private readonly notifier: NotifierService,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      listed: Company[];
      fullset: Company[];
      clientId: string;
    },
  ) {
    const ids = this.data.listed.map((company: Company) => company.id);

    this.companies = this.data.fullset.filter(
      (company: Company) => ids.indexOf(company.id) < 0,
    );

    // this.companies = data.fullset.slice();

    this.dataSource = new MatTableDataSource(this.companies);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public add(id: string): void {
    this.requester
      .post(API_URL('watchlist', 'addcompany'), {
        clientId: this.data.clientId,
        companyId: id,
      })
      .subscribe(
        result => {
          const company = this.companies.find(
            (_company: Company) => _company.id === id,
          );
          this.data.listed.push(company);
          this.companies.splice(this.companies.indexOf(company), 1);
          this.dialogRef.close(true);
        },
        (e: Error) => {
          this.notifier.error(e.message);
        },
      );
  }

  public search(key: string): void {
    this.dataSource.filter = key.trim().toLowerCase();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

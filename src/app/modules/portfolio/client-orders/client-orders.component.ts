import { Component, Input, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Client } from 'src/app/models/client.model';
import { Order } from 'src/app/models/order.model';
import { SocketService } from '../../manager/services/socket.service';
import { UpdaterService } from '../../manager/services/updater.service';
import { Subscription } from 'rxjs';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { API_URL } from 'src/app/common/urls';
import { OrderMode } from 'src/app/common/contracts/order-mode';
import { PortfolioService } from '../../core/portfolio.service';
import { RequestService } from '../../core/request.service';
import { NotifierService } from '../../core/notifier.service';

@Component({
  selector: 'app-client-orders',
  templateUrl: './client-orders.component.html',
  styleUrls: ['./client-orders.component.css']
})
export class ClientOrdersComponent implements OnInit, OnDestroy  {

  private client: Client = null;
  private clients: Client[] = [];
  private clientSubscribtion: Subscription;
  private clientId: string;
  private tableInitialized = false;
  private dataSource;

  public orders: Order[] = [];

  @ViewChild(MatSort) public sort: MatSort;
  @ViewChild(MatPaginator) public paginator: MatPaginator;

  public displayedColumns = [
    'id',
    'company',
    'units',
    'buyprice',
    'profit',
    'close',
  ];

  constructor (
    private readonly socketService: SocketService,
    private readonly updaterService: UpdaterService,
    private readonly portfolioService: PortfolioService,
    private readonly requester: RequestService,
    private readonly notifier: NotifierService,
  ) {
    this.clientId = this.portfolioService.id;

    this.dataSource = new MatTableDataSource(this.orders);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public refreshOrdersTable(): void {
    this.client = this.clients.find((client: Client) => client.id === this.clientId);
    this.orders = this.client.orders;

    this.dataSource = new MatTableDataSource(this.orders);
  }

  public search(key: string): void {
    this.dataSource.filter = key.trim().toLowerCase();
  }

  public close(id: string): void {
    this.requester.post(API_URL('orders', 'sell'), {
      orderId: id,
    }).subscribe(
      (result) => {
        this.filter();
      },
      (error: Error) => { this.notifier.error(error.message); }
    );
  }

  private filter(): void {
    this.orders = this.orders.filter((order: Order) => order.status.statusname !== 'closed');
    this.dataSource = new MatTableDataSource(this.orders);
  }

  public ngOnInit(): void {
    // connect to socket service
    this.socketService.mount();

    this.clientSubscribtion = this.socketService
      .clients()
      .subscribe((clients: Client[]) => {
        this.updaterService.updateclients(this.clients, clients, OrderMode.opened);
        this.filter();
        // updated the table with the new set of available data
        if (!this.tableInitialized && this.clients.length) {
          this.refreshOrdersTable();
          this.tableInitialized = true;
        }
      });
  }

  public ngOnDestroy(): void {
    this.socketService.unmount();
  }

}

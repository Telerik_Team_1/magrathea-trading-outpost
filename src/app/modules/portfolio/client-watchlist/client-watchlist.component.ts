import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { UserOverviewItem } from 'src/app/models/user-overview';
import { ActivatedRoute } from '@angular/router';
import {
  MatSort,
  MatPaginator,
  MatTableModule,
  MatTableDataSource,
  MatDialog,
} from '@angular/material';
import { AddCompanyDialogComponent } from '../add-company-dialog/add-company-dialog.component';
import { SocketService } from '../../manager/services/socket.service';
import { UpdaterService } from '../../manager/services/updater.service';
import { Company } from 'src/app/models/company.model';
import { Subscription } from 'rxjs';
import { CompanyPartial } from 'src/app/models/partial/company-partial.model';
import { Client } from 'src/app/models/client.model';
import { BuyStockDialogComponent } from '../buy-stock-dialog/buy-stock-dialog.component';
import { PortfolioService } from '../../core/portfolio.service';

@Component({
  selector: 'app-client-watchlist',
  templateUrl: './client-watchlist.component.html',
  styleUrls: ['./client-watchlist.component.css'],
})
export class ClientWatchlistComponent implements OnInit, OnDestroy {
  public listed: Company[] = [];
  public fullset: Company[] = [];
  public client: Client;
  public clientId: string;

  private companySubscribtion: Subscription;
  private clientSubscription: Subscription;

  public displayedColumns: string[] = ['market', 'chart', 'sell', 'buy'];
  @ViewChild(MatSort) public sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public dataSource;

  public constructor(
    public dialog: MatDialog,
    private readonly route: ActivatedRoute,
    private readonly socketService: SocketService,
    private readonly updaterService: UpdaterService,
    private readonly portfolioService: PortfolioService,
  ) {
    this.clientId = this.portfolioService.id;
  }

  public ngOnInit(): void {
    this.clientSubscription = this.socketService
      .clients()
      .subscribe((clients: Client[]) => {
        if (!this.client) {
          this.client = clients.find(
            (client: Client) => client.id === this.clientId,
          );
        }
      });

    const companies = this.route.snapshot.data['companies'];

    const ids = companies.map((company: CompanyPartial) => company.id);

    this.socketService.mount();

    this.companySubscribtion = this.socketService
      .companies()
      .subscribe((_companies: Company[]) => {
        this.updaterService.updatecompanies(this.fullset, _companies);

        if (!this.listed.length) {
          this.fullset.forEach((company: Company) => {
            if (ids.indexOf(company.id) >= 0) {
              this.listed.push(company);
            }
          });
          this.refreshClientTable();
        }

        // any update table logic here
      });

    this.dataSource = new MatTableDataSource<Company>(this.listed);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public ngOnDestroy(): void {
    this.companySubscribtion.unsubscribe();
    this.clientSubscription.unsubscribe();
  }

  public get showAddCompany(): boolean {
    return this.listed.length < this.fullset.length;
  }

  public addCompany(): void {
    const dialogRef = this.dialog.open(AddCompanyDialogComponent, {
      width: '90%',
      data: {
        listed: this.listed,
        fullset: this.fullset,
        clientId: this.clientId,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.refreshClientTable();
      }
    });
  }

  public buy(company: Company): void {
    const dialogRef = this.dialog.open(BuyStockDialogComponent, {
      width: '300px',
      data: {
        client: this.client,
        company,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      // any logic here
    });
  }

  public search(key: string): void {
    this.dataSource.filter = key.trim().toLowerCase();
  }

  public refreshClientTable(): void {
    this.dataSource = new MatTableDataSource(this.listed);
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Client } from 'src/app/models/client.model';
import { Company } from 'src/app/models/company.model';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { API_URL } from 'src/app/common/urls';
import { NotifierService } from '../../core/notifier.service';
import { RequestService } from '../../core/request.service';

@Component({
  selector: 'app-buy-stock-dialog',
  templateUrl: 'buy-stock-dialog.component.html',
  styleUrls: ['./buy-stock-dialog.component.css'],
})
export class BuyStockDialogComponent implements OnInit {

  public client: Client;
  public company: Company;

  public buyForm: FormGroup;
  public units: AbstractControl;

  private ticker;

  public constructor(
    private readonly formBuilder: FormBuilder,
    private readonly notifier: NotifierService,
    public dialogRef: MatDialogRef<BuyStockDialogComponent>,
    private readonly requester: RequestService,
    @Inject(MAT_DIALOG_DATA) public data: {
      client: Client,
      company: Company,
    }
  ) {
    this.client = data.client;
    this.company = data.company;
  }

  public ngOnInit(): void {
    this.units = this.formBuilder.control('0', [Validators.required]);
    this.buyForm = this.formBuilder.group({
      units: this.units,
    });
    // this.ticker
  }

  public buy(): void {
    if (this.units.value < 0) {
      this.notifier.warning(`You can't buy less than 0.`);

      return;
    }

    const total = this.company.buy * this.units.value;
    if (total > this.client.funds) {
      this.notifier.warning(`Insufficient funds.`);

      return;
    }
    this.requester.post(API_URL('orders', 'buy'), {
      clientId: this.client.id,
      order: {
        companyId: this.company.id,
        units: + this.units.value,
      }
    }).subscribe(
      (result) => this.dialogRef.close(),
      (error: Error) => { this.notifier.error(error.message); }
    );
  }

  public validateUnits(units: number): string {
    const total = this.company.buy * this.units.value;
    return total > this.client.allocated ? `You don't have sufficient funds` : '';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

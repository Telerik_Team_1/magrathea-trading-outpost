import {
  ActivatedRouteSnapshot,
  Router,
  Resolve
} from '@angular/router';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { API_URL } from 'src/app/common/urls';
import { RequestService } from '../../core/request.service';
import { PortfolioService } from '../../core/portfolio.service';

@Injectable()
export class ClientHistoryResolverService implements Resolve<Observable<any>> {
  public constructor(
    private readonly router: Router,
    private readonly requester: RequestService,
    private readonly portfolioService: PortfolioService,
  ) {}
  public resolve(route: ActivatedRouteSnapshot): Observable<any> {
    const clientId = this.portfolioService.id;
    return this.requester.get(API_URL('orders', 'history', clientId)).pipe(
      tap((data: any) => {
        // any validation or further processing here
      })
    );
  }
}

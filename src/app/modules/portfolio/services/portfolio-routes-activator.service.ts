import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { PortfolioService } from '../../core/portfolio.service';

@Injectable()
export class PortfolioRoutesActivatorService implements CanActivate {

  public constructor (
    private readonly portfolioService: PortfolioService,
    private readonly router: Router,
    ) {}

  public canActivate(): Observable<boolean> {
    return this.portfolioService.isManaging$;
  }

}

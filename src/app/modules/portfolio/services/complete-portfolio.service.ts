import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MatDialog } from '@angular/material';
import { CompletePortfolioDialogComponent } from '../complete-portfolio-dialog/complete-portfolio-dialog.component';
import { PortfolioService } from '../../core/portfolio.service';

@Injectable()
export class CompletePortfolioService {

  public constructor(
    public dialog: MatDialog,
    private readonly portfolioService: PortfolioService,
    ) {}

  public confirm(): Observable<boolean> {
    const dialogRef = this.dialog.open(CompletePortfolioDialogComponent, {width: '300px'});

    return dialogRef.afterClosed();
  }

}

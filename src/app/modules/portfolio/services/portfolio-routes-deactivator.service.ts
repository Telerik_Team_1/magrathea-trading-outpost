import { Injectable, Component } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CompletePortfolioService } from './complete-portfolio.service';
import { PortfolioService } from '../../core/portfolio.service';

@Injectable()
export class PortfolioRoutesDeactivatorService implements CanDeactivate<Component> {

  component: Object;
  route: ActivatedRouteSnapshot;

  public constructor(
    private readonly portfolioService: PortfolioService,
    private readonly completePortfolio: CompletePortfolioService,
  ) {}

  public canDeactivate (
    component: Component,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ): Observable<boolean> | boolean {

    if (nextState.url.startsWith('/portfolio')) {
      return true;
    }

    return this.completePortfolio.confirm().pipe(tap((outcome: boolean) => {
      if (outcome) {
        this.portfolioService.finish();
      }
    }));

    // return this.portfolioService.isManaging$.pipe(map((isManaging) => {
    //   return isManaging ? nextState.url.startsWith('/portfolio') : true;
    // }));
  }

}

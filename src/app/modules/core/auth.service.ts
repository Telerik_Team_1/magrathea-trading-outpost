import { StorageService } from './storage.service';
import { Injectable, OnInit } from '@angular/core';
import { RequestService } from './request.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap, share, catchError } from 'rxjs/operators';
import { UserModel } from 'src/app/models/user.model';
import { API_URL } from 'src/app/common/urls';
import { LoginResponse } from 'src/app/models/login.mode';

@Injectable()
export class AuthService implements OnInit {

  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(
    this.hasToken()
  );

  private readonly roleSubject$ = new BehaviorSubject<string>(
    this.getRole()
  );

  public constructor(
    private readonly storageService: StorageService,
    private readonly requester: RequestService
  ) {}

  public ngOnInit() {
    this.isLoggedInSubject$.next(this.hasToken());
    this.roleSubject$.next(this.getRole());
  }

  public get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  public get role$(): Observable<string> {
    return this.roleSubject$.asObservable();
  }

  public loginUser(user: UserModel): Observable<any> {
    return this.requester
      .post(API_URL('auth', 'login'), user)
      .pipe(
        tap((response: LoginResponse) => {
          this.storageService.setItem('role', response.role);
          this.storageService.setItem('token', response.token);

          this.roleSubject$.next(this.getRole());
          this.isLoggedInSubject$.next(true);
        })
      );
  }

  public logoutUser(): void {
    this.storageService.removeItem('token');
    this.storageService.removeItem('role');
    this.isLoggedInSubject$.next(false);
    this.roleSubject$.next('');
  }

  private hasToken(): boolean {
    return !!this.storageService.getItem('token');
  }

  private getRole(): string {
    return this.storageService.getItem('role') || '';
  }
}

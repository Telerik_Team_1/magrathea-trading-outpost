import { AuthService } from '../auth.service';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NotifierService } from '../notifier.service';

@Injectable()
export class AdminRoutesActivatorService implements CanActivate {
  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notifier: NotifierService
  ) {}

  public canActivate(): Observable<boolean> {
    return this.authService.role$.pipe(
      map((role: string) => {
        if (role !== 'admin') {
          this.router.navigate(['/home']);
          this.notifier.error(
            'You must have admin rights to see this page!'
          );
        }

        return role === 'admin';
      })
    );
  }
}

import { NgModule, Optional, SkipSelf } from '@angular/core';
import { RequestService } from './request.service';
import { StorageService } from './storage.service';
import { NotifierService } from './notifier.service';
import { AuthService } from './auth.service';
import { HttpClientModule } from '@angular/common/http';
import { AdminRoutesActivatorService } from './route-guards/admin-routes-activator.service';
import { AuthRouteActivatorService } from './route-guards/auth-route-activator.service';
import { AnonymousRouteActivatorService } from './route-guards/anonymous-route-activator.service';
import { ManagerRoutesActivatorService } from './route-guards/manager-routes-activator.service';
import { PortfolioService } from './portfolio.service';

@NgModule({
  imports: [HttpClientModule],
  providers: [
    RequestService,
    StorageService,
    NotifierService,
    AuthService,
    PortfolioService,
    AdminRoutesActivatorService,
    AuthRouteActivatorService,
    AnonymousRouteActivatorService,
    ManagerRoutesActivatorService,
  ]
})
export class CoreModule {
  public constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core module is already provided!');
    }
  }
}

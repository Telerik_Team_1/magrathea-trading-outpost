import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { StorageService } from './storage.service';

@Injectable()
export class PortfolioService {

  private clientId: string = null;

  private isManagingSubject$ = new BehaviorSubject<boolean>(this.isManaging());

  constructor(
    private readonly storageService: StorageService,
  ) {}

  public manage(id: string): void {
    this.clientId = id;
    this.storageService.setItem('clientId', id);
    this.isManagingSubject$.next(true);
  }

  private isManaging(): boolean {
    const id = this.storageService.getItem('clientId');
    return id && id.length > 0;
  }

  public get isManaging$(): Observable<boolean> {
    return this.isManagingSubject$.asObservable();
  }

  public finish(): void {
    this.clientId = null;
    this.storageService.setItem('clientId', '');
    this.isManagingSubject$.next(false);
  }

  get id(): string | null {
    return this.storageService.getItem('clientId');
  }

}

import { Injectable } from '@angular/core';
import * as toastr from 'toastr';

const options = {
  positionClass: 'toast-bottom-left',
  closeOnHover: true,
};

@Injectable()
export class NotifierService {
  public success(message: string, title?: string): void {
    toastr.success(message, title, options);
  }

  public error(message: string, title?: string): void {
    toastr.error(message, title, options);
  }

  public warning(message: string, title?: string): void {
    toastr.warning(message, title, options);
  }

  public info(message: string, title?: string): void {
    toastr.info(message, title, options);
  }
}

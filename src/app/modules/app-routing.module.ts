
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from '../appmodule-components/home/home.component';
import { LoginComponent } from '../appmodule-components/home/login/login.component';
import { NotFoundComponent } from '../appmodule-components/not-found/not-found.component';
import { ServerErrorComponent } from '../appmodule-components/server-error/server-error.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },

  { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
  { path: 'manager', loadChildren: './manager/manager.module#ManagerModule' },
  { path: 'portfolio', loadChildren: './portfolio/portfolio.module#PortfolioModule' },

  { path: '404', component: NotFoundComponent },
  { path: 'server-error', component: ServerErrorComponent },

  { path: '**', redirectTo: '/404', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

import { Routes, RouterModule } from '@angular/router';
import { AdminOverviewComponent } from './overview/admin-overview.component';
import { NgModule } from '@angular/core';
import { AdminOverviewResolverService } from './services/admin-overview-resolver';
import { AdminManagersListResolverService } from './services/admin-managers-list-resolver';
import { NewAdminComponent } from './createUser/new-admin/new-admin.component';
import { NewManagerComponent } from './createUser/new-manager/new-manager.component';
import { NewClientComponent } from './createUser/new-client/new-client.component';
import { EditClientComponent } from './editUser/edit-client/edit-client.component';
import { AdminUserEditResolverService } from './services/admin-user-edit-resolver';
import { EditManagerComponent } from './editUser/edit-manager/edit-manager.component';
import { EditAdminComponent } from './editUser/edit-admin/edit-admin.component';
import { AdminRoutesActivatorService } from '../core/route-guards/admin-routes-activator.service';
import { AuthRouteActivatorService } from '../core/route-guards/auth-route-activator.service';

const routes: Routes = [
  {
    path: '',
    component: AdminOverviewComponent,
    canActivate: [AdminRoutesActivatorService, AuthRouteActivatorService],
    resolve: { users: AdminOverviewResolverService },
  },
  {
    path: 'clients/add',
    component: NewClientComponent,
    canActivate: [AdminRoutesActivatorService, AuthRouteActivatorService],
    resolve: { managers: AdminManagersListResolverService },
  },
  {
    path: 'clients/:id/edit',
    component: EditClientComponent,
    canActivate: [AdminRoutesActivatorService, AuthRouteActivatorService],
    resolve: { managers: AdminManagersListResolverService, user: AdminUserEditResolverService, },
  },
  {
    path: 'managers/add',
    component: NewManagerComponent,
    canActivate: [AdminRoutesActivatorService, AuthRouteActivatorService],
  },
  {
    path: 'managers/:id/edit',
    component: EditManagerComponent,
    canActivate: [AdminRoutesActivatorService, AuthRouteActivatorService],
    resolve: { user: AdminUserEditResolverService, },
  },
  {
    path: 'admins/add',
    component: NewAdminComponent,
    canActivate: [AdminRoutesActivatorService, AuthRouteActivatorService],
  },
  {
    path: 'admins/:id/edit',
    component: EditAdminComponent,
    canActivate: [AdminRoutesActivatorService, AuthRouteActivatorService],
    resolve: { user: AdminUserEditResolverService, },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}

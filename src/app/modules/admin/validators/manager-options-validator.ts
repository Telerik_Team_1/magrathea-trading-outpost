import { ValidatorFn, AbstractControl } from '@angular/forms';
import { UserShortOverview } from 'src/app/models/user-short-overview';

export function managerOptionsValidator(options: UserShortOverview[]): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const matches = options.includes(control.value);
    return matches ? null : {'managerOptionsValidator': {value: control.value}};
  };
}

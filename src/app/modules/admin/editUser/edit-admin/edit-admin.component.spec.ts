import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAdminComponent } from './edit-admin.component';
import { AdminOverviewComponent } from '../../overview/admin-overview.component';
import { NewUserComponent } from '../../createUser/new-user/new-user.component';
import { NewClientComponent } from '../../createUser/new-client/new-client.component';
import { NewManagerComponent } from '../../createUser/new-manager/new-manager.component';
import { NewAdminComponent } from '../../createUser/new-admin/new-admin.component';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { EditClientComponent } from '../edit-client/edit-client.component';
import { EditManagerComponent } from '../edit-manager/edit-manager.component';
import { AdminRoutingModule } from '../../admin-routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpHandler, HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { RequestService } from 'src/app/core/request.service';
import { NotifierService } from 'src/app/core/notifier.service';

describe('EditAdminComponent', () => {
  let component: EditAdminComponent;
  let fixture: ComponentFixture<EditAdminComponent>;

  const fakeActivatedRoute = {
    snapshot: {
      data: {
        managers: [],
        user: null,
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AdminOverviewComponent,
        NewUserComponent,
        NewClientComponent,
        NewManagerComponent,
        NewAdminComponent,
        EditUserComponent,
        EditClientComponent,
        EditAdminComponent,
        EditManagerComponent,
      ],
      imports: [
        AdminRoutingModule,
        SharedModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        HttpHandler,
        HttpClient,
        {provide: ActivatedRoute, useValue: fakeActivatedRoute},
        RequestService,
        NotifierService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

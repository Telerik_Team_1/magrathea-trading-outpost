import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, AbstractControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ErrorText } from 'src/app/common/error-texts';
import { startWith, map } from 'rxjs/operators';
import { API_URL } from 'src/app/common/urls';
import { UserShortOverview } from 'src/app/models/user-short-overview';
import * as CustomValidators from '../../validators';
import { RequestService } from 'src/app/modules/core/request.service';
import { NotifierService } from 'src/app/modules/core/notifier.service';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  userId: string;
  user;
  showForm = true;
  maxDOB = new Date();
  editUserForm: FormGroup;
  name: AbstractControl;
  dob: AbstractControl;
  address: AbstractControl;
  email: AbstractControl;
  password: AbstractControl;
  manager = new FormControl();
  options: UserShortOverview[] = [];
  filteredOptions: Observable<UserShortOverview[]>;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly formBuilder: FormBuilder,

    private readonly requestor: RequestService,

    private readonly notifier: NotifierService,

    ) {}

  ngOnInit() {
    this.options = this.route.snapshot.data['managers'];
    this.user = this.route.snapshot.data['user'];
    this.maxDOB.setFullYear(this.maxDOB.getFullYear() - 18);
    this.setOriginalFormValue();

  }
  createForm() {

    this.filteredOptions = this.manager.valueChanges
    .pipe(
      startWith<string | UserShortOverview>(''),
      map(value => typeof value === 'string' ? value : value.name),
      map(name => name ? this._filter(name) : this.options.slice())
      );

    this.name = this.formBuilder.control('', [Validators.required, Validators.pattern(/^[A-Z][A-Za-z ]+$/)]);
    this.email = this.formBuilder.control('', [Validators.required, Validators.email]);
    this.dob = this.formBuilder.control('', [Validators.required]);
    this.address = this.formBuilder.control('', [
      Validators.required,
      Validators.pattern(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])/),
    ]);
    if ( this.user.role !== 'client') {
      this.password = this.formBuilder.control('', [
        Validators.required,
        Validators.pattern(/^(?=.*[a-z])(?=.*[0-9])(?=.{6,})/),
      ]);    } else {
        this.manager = this.formBuilder.control('', [
          Validators.required,
          CustomValidators.managerOptionsValidator(this.options)
        ]);
      }

    this.editUserForm = this.formBuilder.group({
      name: this.name,
      dob: this.dob,
      address: this.address,
      email: this.email,
      password: this.password,
      manager: this.manager,
    });
  }

  displayFn(manager?): string | undefined {
    return manager ? `${manager.name} (${manager.email})` : undefined;
  }

  private _filter(name: string): UserShortOverview[] {
  const filterValue = name.toLowerCase();

  return this.options.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }

  private updateUser(): void {
    const id = this.route.snapshot.params['id'];
    this.requestor.put(API_URL(`users/${id}`), this.editUserForm.value)
    .subscribe(
      (response) => {
      this.notifier.success(`${this.name.value}'s profile was udpated!`);
    },
    error => {
      this.notifier.error('Updating the user failed!', error.error.message);
    });
  }
  private setOriginalFormValue() {
    this.showForm = false;
    setTimeout(() => {
      this.createForm();
      this.editUserForm.patchValue({
        name: this.user.fullname,
        email: this.user.email,
        address: this.user.address,
        dob: this.user.dob.split('T')[0],
      });
        this.showForm = true;
        this.manager.setValue(this.options ?
          this.options.find(option => option.email === this.user.manager.email) :
          this.user.manager
        );
      });
  }
  public validate(control: AbstractControl): string {
    return control.hasError('required') ? ErrorText.requiredField : '';
  }
}

import { AdminOverviewComponent } from './overview/admin-overview.component';
import { AdminRoutingModule } from './admin-routing';
import { NgModule } from '@angular/core';
import { AdminOverviewResolverService } from './services/admin-overview-resolver';
import { CommonModule } from '@angular/common';
import { NewUserComponent } from './createUser/new-user/new-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminManagersListResolverService } from './services/admin-managers-list-resolver';
import { NewClientComponent } from './createUser/new-client/new-client.component';
import { NewManagerComponent } from './createUser/new-manager/new-manager.component';
import { NewAdminComponent } from './createUser/new-admin/new-admin.component';
import { EditUserComponent } from './editUser/edit-user/edit-user.component';
import { EditClientComponent } from './editUser/edit-client/edit-client.component';
import { EditAdminComponent } from './editUser/edit-admin/edit-admin.component';
import { EditManagerComponent } from './editUser/edit-manager/edit-manager.component';
import { AdminUserEditResolverService } from './services/admin-user-edit-resolver';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    AdminOverviewComponent,
    NewUserComponent,
    NewClientComponent,
    NewManagerComponent,
    NewAdminComponent,
    EditUserComponent,
    EditClientComponent,
    EditAdminComponent,
    EditManagerComponent,
  ],
  imports: [
    AdminRoutingModule,
    SharedModule,
    CommonModule,

    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    AdminOverviewResolverService,
    AdminManagersListResolverService,
    AdminUserEditResolverService,
  ]
})
export class AdminModule {}

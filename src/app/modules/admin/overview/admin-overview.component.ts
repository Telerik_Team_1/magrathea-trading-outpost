import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { UserOverviewItem } from 'src/app/models/user-overview';
import { ActivatedRoute } from '@angular/router';
import { MatSort, MatPaginator, MatTableModule, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-admin-overview',
  templateUrl: './admin-overview.component.html',
  styleUrls: ['./admin-overview.component.css']
})
export class AdminOverviewComponent implements OnInit {

  public displayedColumns: string[] = ['name', 'email', 'role'];
  @ViewChild(MatSort) public sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  public dataSource;
  private users: UserOverviewItem[];

  public constructor(
    private readonly route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.users = this.route.snapshot.data['users'];

    this.dataSource = new MatTableDataSource<UserOverviewItem>(this.users);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public search(key: string): void {
    this.dataSource.filter = key.trim().toLowerCase();
  }

  public computeRoute(user): string[] {
    return ['/admin', user.role + 's' , user.id, 'edit'];
  }
}

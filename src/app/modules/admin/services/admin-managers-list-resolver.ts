
import {
  ActivatedRouteSnapshot,
  Router,
  Resolve
} from '@angular/router';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { API_URL } from 'src/app/common/urls';
import { RequestService } from '../../core/request.service';

@Injectable()
export class AdminManagersListResolverService implements Resolve<Observable<any>> {
  public constructor(
    private readonly router: Router,
    private readonly requester: RequestService,
  ) {}
  public resolve(route: ActivatedRouteSnapshot): Observable<any> {

    return this.requester.get(API_URL('users/managers')).pipe(
      tap((data: any) => {
        // any validation or further processing here
      })
    );
  }
}

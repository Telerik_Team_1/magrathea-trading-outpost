import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAdminComponent } from './new-admin.component';
import { NewUserComponent } from '../new-user/new-user.component';
import { AdminRoutingModule } from '../../admin-routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminOverviewComponent } from '../../overview/admin-overview.component';
import { NewClientComponent } from '../new-client/new-client.component';
import { EditClientComponent } from '../../editUser/edit-client/edit-client.component';
import { NewManagerComponent } from '../new-manager/new-manager.component';
import { EditManagerComponent } from '../../editUser/edit-manager/edit-manager.component';
import { EditAdminComponent } from '../../editUser/edit-admin/edit-admin.component';
import { EditUserComponent } from '../../editUser/edit-user/edit-user.component';
import { AdminOverviewResolverService } from '../../services/admin-overview-resolver';
import { AdminManagersListResolverService } from '../../services/admin-managers-list-resolver';
import { AdminUserEditResolverService } from '../../services/admin-user-edit-resolver';
import { ActivatedRoute } from '@angular/router';
import { RequestService } from 'src/app/core/request.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { NotifierService } from 'src/app/core/notifier.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('NewAdminComponent', () => {
  let component: NewAdminComponent;
  let fixture: ComponentFixture<NewAdminComponent>;

  const fakeActivatedRoute = {
    snapshot: {
      data: {
        managers: [],
        user: null,
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AdminOverviewComponent,
        NewUserComponent,
        NewClientComponent,
        NewManagerComponent,
        NewAdminComponent,
        EditUserComponent,
        EditClientComponent,
        EditAdminComponent,
        EditManagerComponent,
      ],
      imports: [
        AdminRoutingModule,
        SharedModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        HttpHandler,
        HttpClient,
        {provide: ActivatedRoute, useValue: fakeActivatedRoute},
        RequestService,
        NotifierService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

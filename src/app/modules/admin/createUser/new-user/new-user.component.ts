import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {FormControl, FormGroup, AbstractControl, FormBuilder, Validators, ValidatorFn} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { UserOverviewItem } from 'src/app/models/user-overview';
import { ActivatedRoute } from '@angular/router';
import { ErrorText } from 'src/app/common/error-texts';
import { API_URL } from 'src/app/common/urls';
import * as CustomValidators from '../../validators/';
import { RequestService } from 'src/app/modules/core/request.service';
import { NotifierService } from 'src/app/modules/core/notifier.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  @Input() role: string;
  maxDOB = new Date();
  showForm = true;
  newUserForm: FormGroup;
  name: AbstractControl;
  dob: AbstractControl;
  address: AbstractControl;
  email: AbstractControl;
  funds: AbstractControl;
  password: AbstractControl;
  manager = new FormControl();
  options: UserOverviewItem[] = [];
  filteredOptions: Observable<UserOverviewItem[]>;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly formBuilder: FormBuilder,

    private readonly requestor: RequestService,

    private readonly notifier: NotifierService,

    ) {}

  ngOnInit() {
    this.maxDOB.setFullYear(this.maxDOB.getFullYear() - 18);

    this.createForm();

    this.filteredOptions = this.manager.valueChanges
    .pipe(
      startWith<string | UserOverviewItem>(''),
      map(value => typeof value === 'string' ? value : value.name),
      map(name => name ? this._filter(name) : this.options.slice())
      );
  }
  createForm() {
    this.options = this.route.snapshot.data['managers'];

    this.name = this.formBuilder.control('', [Validators.required, Validators.pattern(/^[A-Z][A-Za-z ]+$/)]);
    this.email = this.formBuilder.control('', [Validators.required, Validators.email]);
    this.dob = this.formBuilder.control('', [Validators.required]);
    this.address = this.formBuilder.control('', [
      Validators.required,
      Validators.pattern(/^(?=.*[a-z])(?=.*[0-9])/),
  ]);
    if ( this.role !== 'client') {
      this.password = this.formBuilder.control('', [
        Validators.required,
        Validators.pattern(/^(?=.*[a-z])(?=.*[0-9])(?=.{6,})/),
      ]);
    } else {
      this.manager = this.formBuilder.control('', [
        Validators.required,
        CustomValidators.managerOptionsValidator(this.options)
      ]);
      this.funds = this.formBuilder.control('', [Validators.required, Validators.min(1)]);
    }

    this.newUserForm = this.formBuilder.group({
      role: this.role,
      name: this.name,
      dob: this.dob,
      address: this.address,
      email: this.email,
      password: this.password,
      manager: this.manager,
      funds: this.funds,
    });
  }

  displayFn(user?): string | undefined {
    return user ? `${user.name} ${user.email}` : undefined;
  }

  private _filter(name: string): UserOverviewItem[] {
  const filterValue = name.toLowerCase();

  return this.options.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }

  private createUser(): void {
    this.requestor.post(API_URL('users'), this.newUserForm.value)
    .subscribe(
      (response) => {
      const sentenceCaseRole = `${this.role.substr(0, 1).toUpperCase()}${this.role.substring(1, this.role.length)}`;
      this.notifier.success(`${sentenceCaseRole} with email ${this.newUserForm.value.email} was created!`);
      this.resetForm();
    },
    error => {
      this.notifier.error('User creation failed!', error.error.message);
    });
  }
  private resetForm() {
    const role = this.role;
    this.showForm = false;
    setTimeout(() => {
      this.createForm();
        this.showForm = true;
        this.newUserForm.value.role = role;
      });
  }
}

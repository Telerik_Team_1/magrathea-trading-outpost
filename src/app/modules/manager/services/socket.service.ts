import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';
import { Injectable } from '@angular/core';
import { Message } from 'src/app/models/message.model';
import { SOCKET_URL } from 'src/app/common/urls';
import { SocketData } from 'src/app/models/socket-data.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { Company } from 'src/app/models/company.model';
import { Client } from 'src/app/models/client.model';
import { PricePartial } from 'src/app/models/partial/price-partial.model';
import { ClientPartial } from 'src/app/models/partial/client-partial.model';
import { Order } from 'src/app/models/order.model';
import { StorageService } from '../../core/storage.service';

@Injectable()
export class SocketService {
  private socket$: WebSocketSubject<Message>;
  private token: string;
  private readonly companies$ = new BehaviorSubject<Company[]>([]);
  private readonly clients$ = new BehaviorSubject<Client[]>([]);

  private mounted = false;

  constructor(private readonly storageService: StorageService) {
    this.token = this.storageService.getItem('token');

    this.socket$ = new WebSocketSubject(SOCKET_URL);
    this.socket$.subscribe(
      this.normalize.bind(this),
      () => {
        /* silent */
      },
      () => console.warn('Completed!'),
    );
  }

  public mount(): void {
    if (this.mounted) {
      return;
    }

    this.socket$.next({
      event: 'mount',
      data: this.token,
    });

    this.mounted = true;
  }

  public unmount(): void {
    this.socket$.next({
      event: 'unmount',
      data: this.token,
    });

    this.mounted = false;
  }

  public companies(): Observable<Company[]> {
    return this.companies$.asObservable();
  }

  public clients(): Observable<Client[]> {
    return this.clients$.asObservable();
  }

  private normalize(data: SocketData): void {
    if (!data.companies) {
      return;
    }

    // remap companies
    const companies: Company[] = [];
    data.prices.forEach((price: PricePartial) => {
      const company: any = {};
      company.abbr = price.company.abbr;
      company.address = price.company.address;
      company.ceo = price.company.ceo;
      company.icon = price.company.icon;
      company.industry = price.company.industry.name;
      company.name = price.company.name;
      company.lowprice = price.lowprice;
      company.highprice = price.highprice;
      company.startprice = price.startprice;
      company.endprice = price.endprice;
      company.id = price.company.id;
      company.buy = price.buy;
      company.sell = price.sell;
      companies.push(company);
    });

    // remap clients
    const clients: Client[] = [];
    data.clients.forEach((_client: ClientPartial) => {
      let buytotal = 0;
      let selltotal = 0;
      // update money data
      _client.orders.forEach((order: Order) => {
        const company = companies.find(
          (_company: Company) => _company.id === order.company.id,
        );
        if (!company) {
          return;
        }
        if (order.status.statusname === 'closed') {
          return;
        }
        buytotal += order.buyprice * order.units;
        selltotal += order.units * company.sell;
      });
      const client: any = {};
      client.id = _client.id;
      client.fullname = _client.fullname;
      client.funds = _client.funds;
      client.email = _client.email;
      client.orders = _client.orders;
      client.orders.forEach((order: Order) => {
        const company = companies.find((_company: Company) => _company.id === order.company.id);
        const boughtAt = order.units * order.buyprice;
        let sellAt = 0 ;
        if (order.status.statusname === 'closed') {
          sellAt = order.units * order.sellprice;
        } else {
          sellAt = order.units * company.sell;
        }
        order.profit = sellAt - boughtAt;
      });
      client.dateRegistered = _client.dateRegistered;
      client.allocated = +buytotal.toFixed(2);
      client.profit = +(selltotal - buytotal).toFixed(2);
      client.equity = client.funds + client.profit;
      clients.push(client);
    });

    // update companies, clients lists
    this.companies$.next(companies);
    this.clients$.next(clients);
  }
}

import { Injectable } from '@angular/core';
import { Company } from '../../../models/company.model';
import { Client } from '../../../models/client.model';
import { Order } from '../../../models/order.model';
import { OrderMode } from 'src/app/common/contracts/order-mode';

@Injectable()
export class UpdaterService {
  constructor() {}

  public updatecompanies(currentdata: Company[], newdata: Company[]): void {
    newdata.forEach((_company: Company) => {
      const company = currentdata.find(
        (current: Company) => current.id === _company.id,
      );
      if (company) {
        this.mapcompany(company, _company);
      } else {
        currentdata.push(this.mapcompany({}, _company));
      }
    });
  }

  public updateclients(currentdata: Client[], newdata: Client[], mode: OrderMode = OrderMode.all): void {
    newdata.forEach((_client: Client) => {
      const client = currentdata.find(
        (current: Client) => current.id === _client.id,
      );
      if (client) {
        this.mapclient(client, _client, mode);
      } else {
        currentdata.push(this.mapclient({}, _client, mode));
      }
    });
  }

  private updateorder(oldclient: Client, newclient: Client, mode: OrderMode): void {
    if (!oldclient.orders) {
      oldclient.orders = [];
    }
    newclient.orders.forEach((_order: Order) => {
      const order = oldclient.orders.find(
        (current: Order) => current.id === _order.id,
      );
      if (order) {
        this.maporder(order, _order);
      } else {
        if (mode === OrderMode.all) {
          oldclient.orders.push(this.maporder({}, _order));
        } else if (_order.status.statusname === mode.toString()) {
          oldclient.orders.push(this.maporder({}, _order));
        }
      }
    });
  }

  private maporder(oldorder: Order | any, neworder: Order): Order {
    for (const key of Object.keys(neworder)) {
      oldorder[key] = neworder[key];
    }
    // oldorder.buyprice = neworder.buyprice;
    // oldorder.cellprice = neworder.cellprice;
    // oldorder.closedate = neworder.closedate;
    // oldorder.company = neworder.company;
    // oldorder.id = neworder.id;
    // oldorder.opendate = neworder.opendate;
    // oldorder.status = neworder.status;
    // oldorder.units = neworder.units;

    return oldorder;
  }

  private mapclient(oldclient: Client | any, newclient: Client, mode: OrderMode): Client {
    for (const key of Object.keys(newclient)) {
      if (key !== 'orders') {
        oldclient[key] = newclient[key];
      }
    }
    // oldclient.allocated = newclient.allocated;
    // oldclient.dateRegistered = newclient.dateRegistered;
    // oldclient.email = newclient.email;
    // oldclient.equity = newclient.equity;
    // oldclient.fullname = newclient.fullname;
    // oldclient.funds = newclient.funds;
    // oldclient.id = newclient.id;
    // oldclient.profit = newclient.profit;
    this.updateorder(oldclient, newclient, mode);

    return oldclient;
  }

  private mapcompany(oldcompany: Company | any, newcompany: Company): Company {
    for (const key of Object.keys(newcompany)) {
      oldcompany[key] = newcompany[key];
    }
    // oldcompany.abbr = newcompany.abbr;
    // oldcompany.address = newcompany.address;
    // oldcompany.ceo = newcompany.ceo;
    // oldcompany.endprice = newcompany.endprice;
    // oldcompany.highprice = newcompany.highprice;
    // oldcompany.icon = newcompany.icon;
    // oldcompany.id = oldcompany.id;
    // oldcompany.industry = newcompany.industry;
    // oldcompany.lowprice = newcompany.lowprice;
    // oldcompany.name = newcompany.name;
    // oldcompany.startprice = newcompany.startprice;

    return oldcompany;
  }
}

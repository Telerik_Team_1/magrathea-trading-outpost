import { Directive, ElementRef, Input, Renderer2, OnInit } from '@angular/core';

@Directive({
  selector: '[appClientLabel]'
})
export class ClientLabelDirective implements OnInit {
  constructor(private el: ElementRef, private renderer: Renderer2) {}
  @Input('appClientLabel') equity: string;

  ngOnInit() {
    this.setStyles(this.equity, this.el);
  }
  private setStyles(equity: string, element: ElementRef) {
    let fontWeight: number;
    let status: string;
    let link: string;
    const ammount = Number(this.equity);
    if (ammount < 10000) {
      return;
    }
    if (ammount >= 10000) {
      status = 'bronze';
      fontWeight = 500;
      link = 'url(../../../../assets/images/bronze.png)';
    }
    if (ammount >= 50000) {
      status = 'silver';
      fontWeight = 700;
      link = 'url(../../../../assets/images/silver.png)';

    }
    if (ammount >= 100000) {
      status = 'gold';
      fontWeight = 900;
      link = 'url(../../../../assets/images/gold.png)';
    }
   const coin = this.renderer.createElement('div');
   this.renderer.setAttribute(coin, 'title', `${status} client
  $${this.equity}`);

   this.renderer.setStyle(coin, 'height', '1em');
   this.renderer.setStyle(coin, 'width', '1em');
   this.renderer.setStyle(coin, 'margin-left', '.5em');
   this.renderer.setStyle(coin, 'background-image', link);
   this.renderer.setStyle(coin, 'background-size', 'contain');

   this.renderer.appendChild(element.nativeElement, coin );

   this.renderer.setStyle(element.nativeElement, 'font-weight', fontWeight);

  }

}


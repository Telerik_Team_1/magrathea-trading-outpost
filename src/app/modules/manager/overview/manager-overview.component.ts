import { UpdaterService } from './../services/updater.service';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { SocketService } from '../services/socket.service';
import { Subscription, of, Observable } from 'rxjs';
import { Company } from 'src/app/models/company.model';
import { Client } from 'src/app/models/client.model';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { PortfolioService } from '../../core/portfolio.service';

@Component({
  selector: 'app-manager-overview',
  templateUrl: './manager-overview.component.html',
})
export class ManagerOverviewComponent implements OnInit, OnDestroy {
  private companySubscribtion: Subscription;
  private clientSubscribtion: Subscription;
  public companies: Company[] = [];
  public clients: Client[] = [];

  private tableInitialized = false;

  public displayedColumns = [
    'fullname',
    'funds',
    'allocated',
    'profit',
    'equity',
    'id',
  ];
  @ViewChild(MatSort) public sort: MatSort;
  @ViewChild(MatPaginator) public paginator: MatPaginator;

  private dataSource;

  private clienTableSub: Subscription;

  public constructor(
    private readonly socketService: SocketService,
    private readonly updaterService: UpdaterService,
    private readonly portfolioService: PortfolioService,
    private readonly router: Router
  ) {}

  public ngOnInit() {
    // initialize the table
    this.dataSource = new MatTableDataSource(this.clients);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

    // update the table with initial data if any
    this.clienTableSub = of(this.clients).subscribe(() =>
      this.refreshClientTable(),
    );

    // connect to socket service
    this.socketService.mount();

    // listen for updates
    this.companySubscribtion = this.socketService
      .companies()
      .subscribe((companies: Company[]) => {
        this.updaterService.updatecompanies(this.companies, companies);
      });
    this.clientSubscribtion = this.socketService
      .clients()
      .subscribe((clients: Client[]) => {
        this.updaterService.updateclients(this.clients, clients);
        // updated the table with the new set of available data
        if (!this.tableInitialized && this.clients.length) {
          this.refreshClientTable();
          this.tableInitialized = true;
        }
      });
  }

  public refreshClientTable(): void {
    this.dataSource = new MatTableDataSource(this.clients);
  }

  public manage(id: string): void {
    this.portfolioService.manage(id);
    this.router.navigate(['/portfolio']);
  }

  public ngOnDestroy() {
    this.socketService.unmount();
    this.companySubscribtion.unsubscribe();
    this.clientSubscribtion.unsubscribe();
    this.clienTableSub.unsubscribe();
  }

  public search(key: string): void {
    this.dataSource.filter = key.trim().toLowerCase();
  }
}

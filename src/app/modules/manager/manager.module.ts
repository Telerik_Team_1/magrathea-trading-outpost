import { NgModule } from '@angular/core';
import { ManagerPositionsComponent } from './positions/manager-positions.component';
import { ManagerRoutingModule } from './manager-routing';
import { SocketService } from './services/socket.service';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { UpdaterService } from './services/updater.service';
import { ClientListComponent } from './client-list/client-list.component';
import { StockchartComponent } from './stockchart/stockchart.component';
import { StocksListComponent } from './stocks-list/stocks-list.component';
import { PricesDataResolver } from './services/prices-data-resolver';
import { ClientLabelDirective } from './directives/client-label.directive';
import { ClientPreviewDialogComponent } from './client-preview-dialog/client-preview-dialog.component';

@NgModule({
  declarations: [
    ManagerPositionsComponent,
    ClientListComponent,
    StockchartComponent,
    StocksListComponent,
    ClientLabelDirective,
    ClientPreviewDialogComponent,
  ],
  imports: [
    ManagerRoutingModule,
    CommonModule,
    SharedModule,
  ],
  providers: [
    SocketService,
    UpdaterService,
    PricesDataResolver,
  ],
  entryComponents: [
    ClientPreviewDialogComponent,
  ]
})
export class ManagerModule {}

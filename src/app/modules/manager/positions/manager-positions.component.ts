import { UpdaterService } from '../services/updater.service';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { SocketService } from '../services/socket.service';
import { Subscription, of, Observable } from 'rxjs';
import { Client } from 'src/app/models/client.model';
import { MatSort, MatPaginator, MatTableDataSource, MatDialogRef, MatDialog } from '@angular/material';
import { OrderMode } from '../../../common/contracts/order-mode';
import { NgxSpinnerService } from 'ngx-spinner';
import { ClientPreviewDialogComponent } from '../client-preview-dialog/client-preview-dialog.component';
import { AddCompanyDialogComponent } from '../../portfolio/add-company-dialog/add-company-dialog.component';

@Component({
  selector: 'app-manager-positions',
  templateUrl: './manager-positions.component.html',
  styleUrls: ['./manager-positions.component.css'],
})
export class ManagerPositionsComponent implements OnInit, OnDestroy {


  private clients: Client[] = [];

  private positionStatus: OrderMode = OrderMode.opened;
  private clientSubscribtion: Subscription;
  private tableInitialized = false;
  private reset = false;
  private dataSource;

  public orders = [];

  @ViewChild(MatSort) public sort: MatSort;
  @ViewChild(MatPaginator) public paginator: MatPaginator;

  public displayedColumns = [
    'localId',
    'companyName',
    'units',
    'buyprice',
    'profit',
    'clientName'
  ];

  constructor (
    private readonly socketService: SocketService,
    private readonly updaterService: UpdaterService,
    private spinner: NgxSpinnerService,
    public dialog: MatDialog,
      ) {
    this.dataSource = new MatTableDataSource(this.orders);
  }

  public updateOrders(): void {
    this.clients.forEach(client => {
      if (client.orders) {
        client.orders.forEach(nOrder => {
          const order = this.orders.find((cOrder) => cOrder.id === nOrder.id);
          if (order) {
            Object.keys(nOrder).forEach(key => {
              order[key] = nOrder[key];
            });
          } else {
            const newOrder = {
              ...nOrder,
              statusname: nOrder.status.statusname,
              companyName: nOrder.company.name,
              clientName: client.fullname,
              client: {id: client.id, fullname: client.fullname},
              localId: this.orders.length,
            };
            this.orders.push(newOrder);
          }
        });
      }
    });
  }
public refreshTable() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.tableInitialized = true;
  }
public resetTable() {
    this.dataSource = new MatTableDataSource(this.orders);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.tableInitialized = true;
    this.reset = false;
  }

  public openPreview(id: string): void {
    const client = this.clients.find((_client: Client) => _client.id === id);

    const dialogRef = this.dialog.open(ClientPreviewDialogComponent, {
      width: '90%',
      data: {
        client,
        orders: client.orders,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      // nothing
    });
  }

  public ngOnInit(): void {
    this.spinner.show();
    // connect to socket service
    this.socketService.mount();

    this.clientSubscribtion = this.socketService
    .clients()
    .subscribe((clients: Client[]) => {
        this.updaterService.updateclients(this.clients, clients, this.positionStatus);
        this.updateOrders();
        if (this.reset) {
          this.resetTable();
        }
        this.refreshTable();
        if (this.clients.length) {
          this.spinner.hide();
        }
      });
  }
  public changeQueryParam(status: string) {
    this.spinner.show();
    this.positionStatus = {
      opened: OrderMode.opened,
      closed: OrderMode.closed,
      all: OrderMode.all
    }[status];
    this.tableInitialized = false;
    this.clients = [];
    this.orders = [];
    this.setShowStatus();
    this.reset = true;
  }

  public setShowStatus() {
    const shouldShow = this.positionStatus.toString() === 'all';
    if (shouldShow) {
      if (this.displayedColumns.slice(-1)[0] !== 'statusname') {
        this.displayedColumns.push('statusname');
      }
      return 'block';
    } else {
      if (this.displayedColumns.slice(-1)[0] === 'statusname') {
        this.displayedColumns.pop();
      }
      return 'none';
    }
  }
  public search(key: string): void {
    this.dataSource.filter = key.trim().toLowerCase();
  }
  public ngOnDestroy(): void {
    this.socketService.unmount();
    this.clientSubscribtion.unsubscribe();
  }
}

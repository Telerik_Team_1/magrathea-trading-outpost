import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription, of } from 'rxjs';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { SocketService } from '../services/socket.service';
import { UpdaterService } from '../services/updater.service';
import { Company } from 'src/app/models/company.model';

@Component({
  selector: 'app-stocks-list',
  templateUrl: './stocks-list.component.html',
  styleUrls: ['./stocks-list.component.css']
})
export class StocksListComponent implements OnInit, OnDestroy {

  private companySubscribtion: Subscription;
  public companies: Company[] = [];
  private tableInitialized = false;

  public displayedColumns = [
    'market',
    'chart',
    'sell',
    'buy',
    'industry',
  ];
  @ViewChild(MatSort) public sort: MatSort;
  @ViewChild(MatPaginator) public paginator: MatPaginator;

  private dataSource;

  private companyTableSub: Subscription;

  public constructor(
    private readonly socketService: SocketService,
    private readonly updaterService: UpdaterService,
  ) {}

  public ngOnInit() {
    this.dataSource = new MatTableDataSource(this.companies);
    this.dataSource.sort = this.sort;
    setTimeout(() => this.dataSource.paginator = this.paginator, 10);

    this.companyTableSub = of(this.companies).subscribe(() =>
      this.refreshCompanyTable(),
    );

    this.socketService.mount();
    this.companySubscribtion = this.socketService
      .companies()
      .subscribe((companies: Company[]) => {
        this.updaterService.updatecompanies(this.companies, companies);
        if (!this.tableInitialized && this.companies.length) {
          this.refreshCompanyTable();
          this.tableInitialized = true;
        }
      });
  }

  public refreshCompanyTable(): void {
    this.dataSource = new MatTableDataSource(this.companies);
  }

  public ngOnDestroy() {
    this.socketService.unmount();
    this.companySubscribtion.unsubscribe();
    this.companyTableSub.unsubscribe();
  }

  public search(key: string): void {
    this.dataSource.filter = key.trim().toLowerCase();
  }
}

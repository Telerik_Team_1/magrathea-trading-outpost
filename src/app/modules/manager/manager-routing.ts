import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ManagerPositionsComponent } from './positions/manager-positions.component';
import { ClientListComponent } from './client-list/client-list.component';
import { StocksListComponent } from './stocks-list/stocks-list.component';
import { StockchartComponent } from './stockchart/stockchart.component';
import { PricesDataResolver } from './services/prices-data-resolver';
import { ManagerRoutesActivatorService } from '../core/route-guards/manager-routes-activator.service';
import { AuthRouteActivatorService } from '../core/route-guards/auth-route-activator.service';

const routes: Routes = [
  {
    path: '',
    component: ManagerPositionsComponent,
    canActivate: [ManagerRoutesActivatorService, AuthRouteActivatorService]
    // resolve: { posts: __MISING_RESOLVER__ }
  }, {
    path: 'positions',
    component: ManagerPositionsComponent,
    canActivate: [ManagerRoutesActivatorService, AuthRouteActivatorService]
    // resolve: { posts: __MISING_RESOLVER__ }
  }, {
    path: 'clients',
    component: ClientListComponent,
    canActivate: [ManagerRoutesActivatorService, AuthRouteActivatorService]
    // resolve: { posts: __MISING_RESOLVER__ }
  },
  {
    path: 'stocks',
    component: StocksListComponent,
    canActivate: [ManagerRoutesActivatorService, AuthRouteActivatorService]
    // resolve: { posts: __MISING_RESOLVER__ }
  }, {
    path: 'stocks/:id',
    component: StockchartComponent,
    canActivate: [ManagerRoutesActivatorService, AuthRouteActivatorService],
    resolve: { prices: PricesDataResolver }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagerRoutingModule {}

import { UpdaterService } from './../services/updater.service';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { SocketService } from '../services/socket.service';
import { Subscription, of, Observable } from 'rxjs';
import { Client } from 'src/app/models/client.model';
import { MatSort, MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ClientPreviewDialogComponent } from '../client-preview-dialog/client-preview-dialog.component';
import { PortfolioService } from '../../core/portfolio.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css'],
})
export class ClientListComponent implements OnInit, OnDestroy {
  private clientSubscribtion: Subscription;
  public clients: Client[] = [];

  private tableInitialized = false;

  public displayedColumns = [
    'fullname',
    'funds',
    'allocated',
    'profit',
    'equity',
    'id'
  ];
  @ViewChild(MatSort) public sort: MatSort;
  @ViewChild(MatPaginator) public paginator: MatPaginator;

  private dataSource;

  private clienTableSub: Subscription;

  public constructor(
    private readonly socketService: SocketService,
    private readonly updaterService: UpdaterService,
    private readonly portfolioService: PortfolioService,
    private readonly router: Router,
    public dialog: MatDialog,
  ) {}

  public ngOnInit() {
    this.dataSource = new MatTableDataSource(this.clients);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

    this.clienTableSub = of(this.clients).subscribe(() =>
      this.refreshTable(),
    );

    this.socketService.mount();
    this.clientSubscribtion = this.socketService
      .clients()
      .subscribe((clients: Client[]) => {
        this.updaterService.updateclients(this.clients, clients);
        if (!this.tableInitialized && this.clients.length) {
          this.refreshTable();
          this.tableInitialized = true;
        }
      });
  }
  public refreshTable() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  public manage(id: string, event): void {
    this.portfolioService.manage(id);
    this.router.navigate(['/portfolio']);
  }


  public ngOnDestroy() {
    this.socketService.unmount();
    this.clientSubscribtion.unsubscribe();
    this.clienTableSub.unsubscribe();
  }

  public search(key: string): void {
    this.dataSource.filter = key.trim().toLowerCase();
  }

  public openPreview(id: string): void {
    const client = this.clients.find((_client: Client) => _client.id === id);

    const dialogRef = this.dialog.open(ClientPreviewDialogComponent, {
      width: '90%',
      data: {
        client,
        orders: client.orders,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      // nothing
    });
  }
}

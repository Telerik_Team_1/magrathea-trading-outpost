import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockchartComponent } from './stockchart.component';
import { ManagerPositionsComponent } from '../positions/manager-positions.component';
import { ClientListComponent } from '../client-list/client-list.component';
import { StocksListComponent } from '../stocks-list/stocks-list.component';
import { ClientLabelDirective } from '../directives/client-label.directive';
import { ManagerRoutingModule } from '../manager-routing';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { SocketService } from '../services/socket.service';
import { UpdaterService } from '../services/updater.service';
import { PricesDataResolver } from '../services/prices-data-resolver';
import { HttpHandler, HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { RequestService } from 'src/app/core/request.service';
import { NotifierService } from 'src/app/core/notifier.service';
import { StorageService } from 'src/app/core/storage.service';

describe('StockchartComponent', () => {
  let component: StockchartComponent;
  let fixture: ComponentFixture<StockchartComponent>;

  const fakeActivatedRoute = {
    snapshot: {
      data: {
        managers: [],
        user: null,
        prices: [{
          id: 'test',
          highprice: 100,
          lowprice: 90,
          startprice: 100,
          endprice: 90,
        }]
      },
      params: {
        id: 'dummyID',
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ManagerPositionsComponent,
        ClientListComponent,
        StockchartComponent,
        StocksListComponent,
        ClientLabelDirective,
      ],
      imports: [
        ManagerRoutingModule,
        CommonModule,
        SharedModule,
      ],
      providers: [
        SocketService,
        UpdaterService,
        PricesDataResolver,
        HttpHandler,
        HttpClient,
        {provide: ActivatedRoute, useValue: fakeActivatedRoute},
        RequestService,
        NotifierService,
        StorageService,
        SocketService,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

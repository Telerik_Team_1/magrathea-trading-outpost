import { Component, OnInit, OnDestroy } from '@angular/core';
import * as CanvasJS from '../../../common/canvasjs.min';
import { Subscription } from 'rxjs';
import { Company } from 'src/app/models/company.model';
import { ActivatedRoute } from '@angular/router';
import { SocketService } from '../services/socket.service';
import { UpdaterService } from '../services/updater.service';
import { PricePartial } from 'src/app/models/partial/price-partial.model';
@Component({
  selector: 'app-stockchart',
  templateUrl: './stockchart.component.html',
  styleUrls: ['./stockchart.component.css']
})
export class StockchartComponent implements OnInit, OnDestroy {
  public company;
  public companySubscribtion: Subscription;
  public companies: Company[] = [];
  public dataPoints = [];
  public currentDataPoint = {
    x: new Date(),
    y: [],
    type: 'const',
  };
  public chart;

  public constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly socketService: SocketService,
    private readonly updaterService: UpdaterService,
  ) {}
  ngOnInit() {

    const id = this.activatedRoute.snapshot.params['id'];

    // temporary
    const prices = this.activatedRoute.snapshot.data['prices'];
    this.feedPrices(prices, this.dataPoints);
    this.currentDataPoint = this.dataPoints[this.dataPoints.length - 1];
    // temporary

    this.socketService.mount();
    this.companySubscribtion = this.socketService
    .companies()
    .subscribe((companies: Company[]) => {
      if (companies.length) {
        const wantedCompanyArray = companies.filter(company => company.id === id);
        this.updaterService.updatecompanies(this.companies, wantedCompanyArray);
        this.company = this.companies[0];
        if (!this.chart) {
          this.createTable();
          }
        this.processIncomingPrices(this.company);
        this.chart.render();
        }
      });

  }

  public processIncomingPrices(company: Company): void {
    if (this.dataPoints.length && this.dataPoints[this.dataPoints.length - 1].type === 'temp') {
      this.dataPoints.pop();
    }

    if (new Date().getTime() > this.currentDataPoint.x.getTime()) {

      this.dataPoints.push({
        x: new Date(this.currentDataPoint.x.getTime()),
        y: [...this.currentDataPoint.y],
        type: 'const',
      });
      this.currentDataPoint = {
        x: new Date(new Date(new Date().setSeconds(0)).setMilliseconds(0) + 60 * 1000),
        y: [
          company.startprice,
          company.highprice,
          company.lowprice,
          company.endprice,
        ],
        type: 'temp',
      };
    } else {

      this.currentDataPoint.y[1] = Math.max(company.highprice, this.currentDataPoint.y[1]);
      this.currentDataPoint.y[2] = Math.min(company.lowprice, this.currentDataPoint.y[2]);
      this.currentDataPoint.y[3] = company.endprice;
      this.dataPoints.push(this.currentDataPoint);
    }

    while (this.dataPoints.length > 60) {
      this.dataPoints.shift();
    }
  }

  public createTable(): void {
    this.chart = new CanvasJS.Chart('chartContainer', {
      animationEnabled: true,
      theme: 'light1', // "light1", "light2", "dark1", "dark2"
      exportEnabled: true,
      title: {
        text: this.company.name
      },
      zoomEnabled: true,
      subtitles: [{
        text: 'Minute Averages for last hour'
      }],
      axisX: {
        title: 'Time',
        interval: 1,
        valueFormatString: 'm',
      },
      axisY: {
        includeZero: false,
        prefix: '$',
        title: 'Price'
      },
      toolTip: {
        content: 'Date: {x}<br /><strong>Price:</strong><br />Open: {y[0]}, Close: {y[3]}<br />High: {y[1]}, Low: {y[2]}'
      },
      data: [{
        type: 'candlestick',
        risingColor: 'green',
        color: 'purple',
        yValueFormatString: '$##0.00',
        dataPoints: this.dataPoints
      }]
    });
    this.chart.render();
  }
  public feedPrices(prices: PricePartial[], chartData: any[]): void {
    prices.reverse().forEach((price) => {
      const dataPoint = {
        x: new Date(price.opendate),
        y: [
          parseFloat(price.startprice + ''),
          parseFloat(price.highprice + ''),
          parseFloat(price.lowprice + ''),
          parseFloat(price.endprice + '')
        ],
        type: 'const'
      };
      chartData.push(dataPoint);
    });
  }
  public ngOnDestroy() {
  this.socketService.unmount();
  this.companySubscribtion.unsubscribe();
  }
}

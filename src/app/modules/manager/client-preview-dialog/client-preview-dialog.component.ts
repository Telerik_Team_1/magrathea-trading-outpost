import { Component, Inject, ViewChild } from '@angular/core';

import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSort,
  MatPaginator,
  MatTableDataSource,
} from '@angular/material';
import { Order } from 'src/app/models/order.model';
import { Client } from 'src/app/models/client.model';
import { RequestService } from '../../core/request.service';

@Component({
  selector: 'app-client-preview-dialog',
  templateUrl: 'client-preview-dialog.component.html',
  styleUrls: ['./client-preview-dialog.component.css'],
})
export class ClientPreviewDialogComponent {

  public client: Client;
  public orders: Order[] = [];

  private dataSource;
  @ViewChild(MatSort) public sort: MatSort;
  @ViewChild(MatPaginator) public paginator: MatPaginator;

  public displayedColumns = ['id', 'company', 'units', 'buyprice', 'profit'];

  constructor(
    public dialogRef: MatDialogRef<ClientPreviewDialogComponent>,
    private readonly requester: RequestService,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      client: Client;
      orders: Order[];
    },
  ) {

    this.client = data.client;
    this.orders = data.orders;

    this.dataSource = new MatTableDataSource(this.orders);
    this.dataSource.sort = this.sort;
    setTimeout(() => this.dataSource.paginator = this.paginator);
  }

  public search(key: string): void {
    this.dataSource.filter = key.trim().toLowerCase();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

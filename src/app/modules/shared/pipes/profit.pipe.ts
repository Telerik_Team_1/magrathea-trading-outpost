import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'profit' })
export class ProfitPipe implements PipeTransform {
  transform(value: string) {
    return value.startsWith('-') ? `<span class="profit-red">${value}</span>` : `<span class="profit-green">${value}</span>`;
  }
}

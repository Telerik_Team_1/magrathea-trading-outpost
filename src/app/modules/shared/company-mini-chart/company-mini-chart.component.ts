import { Component, Input, OnInit } from '@angular/core';
import { Company } from 'src/app/models/company.model';
import { Chart } from 'chart.js';
import { v4 as uuid } from 'uuid';
import { API_URL } from 'src/app/common/urls';
import { RequestService } from 'src/app/modules/core/request.service';
import { NotifierService } from 'src/app/modules/core/notifier.service';

@Component({
  selector: 'app-company-mini-chart',
  templateUrl: './company-mini-chart.component.html',
  styleUrls: ['./company-mini-chart.component.css']
})
export class CompanyMiniChartComponent implements OnInit  {

  private data: any;
  private dataLabels: any[];
  private chart: Chart;
  public id: string;

  @Input() public company: Company;

  public constructor (
    private readonly requester: RequestService,
    private readonly notifier: NotifierService,
  ) {
    this.id = `canvas${uuid()}`;
  }

  public ngOnInit(): void {

    this.data = Array.from({length: 20}).map(() => 0);
    this.dataLabels = Array.from({length: 20}).map((_, ind) => ind);
    setTimeout(this.renderChart.bind(this));

    this.requester.post(API_URL('prices', 'company'), {
      id: this.company.id,
      type: 'day',
    }).subscribe(
      (data) => {
        this.data = data;
        // this.dataLabels = Array.from({length: 20}).map((_, ind) => ind);
        this.renderChart();
      },
      (e: Error) => { this.notifier.error(e.message); },
    );

  }

  public renderChart(): void {
    this.chart = new Chart(this.id, {
      type: 'line',
      data: {
        labels: this.dataLabels,
        datasets: [{
          data: this.data,
          borderColor: '#4cc6ff',
          fill: 'origin',
          borderWidth: 1,
          hideInLegendAndTooltip: true,
        }],
      },
      options: {
        elements: {
          point: {
            radius: 0,
          },
        },
        legend: {
          display: false,
        },
        scales: {
          xAxes: [{
            display: false
          }],
          yAxes: [{
            display: false
          }],
        },
      }
    });
  }

}

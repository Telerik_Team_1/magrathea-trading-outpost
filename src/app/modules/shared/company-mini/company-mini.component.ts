import { Component, Input } from '@angular/core';
import { Company } from 'src/app/models/company.model';
import { API_URL } from 'src/app/common/urls';

@Component({
  selector: 'app-company-mini',
  templateUrl: './company-mini.component.html',
  styleUrls: ['./company-mini.component.css']
})
export class CompanyMiniComponent  {

  @Input() public company: Company;

  constructor () {
  }

  public url(logo: string): string {
    return API_URL('images', logo);
  }

}

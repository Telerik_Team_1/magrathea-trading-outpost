import { NgModule } from '@angular/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { CompanyMiniChartComponent } from './company-mini-chart/company-mini-chart.component';
import { CompanyMiniComponent } from './company-mini/company-mini.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ProfitPipe } from './pipes/profit.pipe';
import { MaterialModule } from '../angular-material.module';

@NgModule({
  imports: [
    DragDropModule,
    MaterialModule,
    NgxSpinnerModule,
  ],
  exports: [
    DragDropModule,
    CommonModule,
    MaterialModule,
    CompanyMiniChartComponent,
    CompanyMiniComponent,
    NgxSpinnerModule,
    ProfitPipe,
  ],
  declarations: [
    CompanyMiniComponent,
    CompanyMiniChartComponent,
    ProfitPipe,
  ],
})
export class SharedModule {}

export enum OrderMode {
  closed = 'closed',
  opened = 'opened',
  all = 'all',
}

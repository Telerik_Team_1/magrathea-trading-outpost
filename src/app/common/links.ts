const BASE = 'http://localhost:5000/';

export const adminLinks = [
  {
     name: 'Add Client',
     route: ['/', 'admin', 'clients', 'add'],
     iconName: 'person_add',
  },
  {
     name: 'Add Manager',
     route: ['/', 'admin', 'managers', 'add'],
     iconName: 'person_add',

  },
  {
     name: 'Add Admin',
     route: ['/', 'admin', 'admins', 'add'],
     iconName: 'person_add',

  },
  {
    name: 'Userlist',
    route: ['/', 'admin'],
    iconName: 'people',
  },
];

export const managerLinks = [
  {
    name: 'Positions',
    route: ['/manager', 'positions'],
    iconName: 'assignments',
  },
  {
    name: 'Stocks',
    route: ['/manager', 'stocks'],
    iconName: 'show_chart',
  },
  {
    name: 'Clients',
    route: ['/manager', 'clients'],
    iconName: 'people_outline',
  },
];

export const portfolioLinks = [
  {
    name: 'Overview',
    route: ['/', 'portfolio'],
    iconName: 'account_circle',
  },
  {
    name: 'Active trades',
    route: ['/', 'portfolio', 'orders'],
    iconName: 'toc',
  },
  {
    name: 'Watchlist',
    route: ['/', 'portfolio', 'watchlist'],
    iconName: 'visibility',
  },
  {
    name: 'History',
    route: ['/', 'portfolio', 'history'],
    iconName: 'history',
  }
];

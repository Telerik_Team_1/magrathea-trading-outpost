const BASE = 'http://localhost:5000/';

export const API_URL = (...parts): string => `${BASE}${parts.join('/')}`;
export const SOCKET_URL = 'ws://localhost:8081/';

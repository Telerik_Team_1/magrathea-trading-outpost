import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/modules/core/auth.service';
import { PortfolioService } from 'src/app/modules/core/portfolio.service';
import { NotifierService } from 'src/app/modules/core/notifier.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

  public loggedIn = false;
  public isManaging = false;
  public role = '';
  public manageButtonText = 'Overview';
  private loggerSubscribtion: Subscription;
  private roleSubscribtion: Subscription;
  private manageSubscribtion: Subscription;

  public constructor (
    private authService: AuthService,
    private readonly router: Router,
    private readonly portfolioService: PortfolioService,
    private readonly notifier: NotifierService,
    ) { }

  public ngOnInit() {
    this.loggerSubscribtion = this.authService.isLoggedIn$.subscribe((loggedIn) => {
      this.loggedIn = loggedIn;
    });
    this.roleSubscribtion = this.authService.role$.subscribe((role) => {
      this.role = role;
    });
    this.manageSubscribtion = this.portfolioService.isManaging$.subscribe(
      this.update.bind(this),
      (e: Error) => this.notifier.error(e.message));
  }

  public ngOnDestroy() {
    this.loggerSubscribtion.unsubscribe();
    this.roleSubscribtion.unsubscribe();
    this.manageSubscribtion.unsubscribe();
  }

  public logOut() {
    this.authService.logoutUser();
    this.router.navigate(['/home']);
  }

  public update(isManaging: boolean): void {
    this.manageButtonText = isManaging ? 'Complete client management' : 'Overview';
  }

}

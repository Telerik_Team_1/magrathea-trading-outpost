import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { ErrorText } from 'src/app/common/error-texts';
import { LoginResponse } from 'src/app/models/login.mode';
import { AuthService } from 'src/app/modules/core/auth.service';
import { NotifierService } from 'src/app/modules/core/notifier.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public email: AbstractControl;
  public password: AbstractControl;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,

    private readonly notifier: NotifierService,
    private readonly formBuilder: FormBuilder,
  ) { }

  ngOnInit() {

    this.email = this.formBuilder.control('', [Validators.required, Validators.email]);
    this.password = this.formBuilder.control('', [
      Validators.required,
    ]);
    this.loginForm = this.formBuilder.group({
      email: this.email,
      password: this.password,
    });

  }

  public login(): void {
    this.authService.loginUser(this.loginForm.value).subscribe(
      (response: LoginResponse) => {
        this.notifier.success('Logged in successfully!');
        if (['admin', 'manager'].includes(response.role)) {
          this.router.navigate(['/', response.role]);
          return;
        }
        this.router.navigate(['/home']);
      },
      error => {
        this.notifier.error('Reason...', 'Login failed!');
      }
    );
  }

  public validate(control: AbstractControl): string {
    return control.hasError('required') ? ErrorText.requiredField : '';
  }

  public cancel(): void {
    this.router.navigate(['/home']);
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from 'src/app/modules/core/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent /* implements OnInit, OnDestroy */ {

  // public loggedIn = false;
  // public role = '';
  // private loggerSubscribtion: Subscription;
  // private roleSubscribtion: Subscription;

  constructor (private authService: AuthService, private readonly router: Router) {
  }

  // public ngOnInit() {
  //   this.loggerSubscribtion = this.authService.isLoggedIn$.subscribe((loggedIn) => {
  //     this.loggedIn = loggedIn;
  //   });
  //   this.roleSubscribtion = this.authService.role$.subscribe((role) => {
  //     this.role = role;
  //   });
  // }

  // public ngOnDestroy() {
  //   this.loggerSubscribtion.unsubscribe();
  //   this.roleSubscribtion.unsubscribe();
  // }

  // public logOut() {
  //   this.authService.logoutUser();
  // }

}

import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Subscription } from 'rxjs';
import { adminLinks, managerLinks, portfolioLinks} from 'src/app/common/links';
import { MatSidenav } from '@angular/material';
import { AuthService } from 'src/app/modules/core/auth.service';
import { PortfolioService } from 'src/app/modules/core/portfolio.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit, OnDestroy {

  @ViewChild('snav') snav: MatSidenav;
  mobileQuery: MediaQueryList;
  private roleSubscribtion: Subscription;
  private managingSubscription: Subscription;
  isLoggedInSubscription: Subscription;
  role: string;
  isLoggedIn: boolean;
  isManaging = false;
  links: Object[];
  mobile = false;

  private _mobileQueryListener: () => void;

  constructor(
    private readonly authService: AuthService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly media: MediaMatcher,
    private readonly portfolioService: PortfolioService,
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.mobile = (/android|webos|iphone|ipad|ipod|blackberry|windows phone/).test(navigator.userAgent.toLowerCase());
  }

  ngOnInit(): void {
    this.managingSubscription = this.portfolioService.isManaging$.subscribe((isManaging) => {
      this.update(this.role, isManaging);
    });

    this.roleSubscribtion = this.authService.role$.subscribe((role) => {
      this.update(role, this.isManaging);
    });
    this.isLoggedInSubscription = this.authService.isLoggedIn$.subscribe((isLoggedIn) => {
      this.isLoggedIn = isLoggedIn;
      if (isLoggedIn) {
        this.snav.open();
      } else {
        this.snav.close();
      }
    });
  }

  private update(role: string, isManaging: boolean): void {

    this.role = role;
    this.isManaging = isManaging;

    if (role === 'admin') {
      this.links = adminLinks;

      return;
    }
    if (isManaging) {
      this.links = portfolioLinks;
    } else {
      this.links = managerLinks;
    }
  }

  isMobile(): boolean {
    return this.mobile;
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.links, event.previousIndex, event.currentIndex);

    this.roleSubscribtion.unsubscribe();
    this.isLoggedInSubscription.unsubscribe();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}

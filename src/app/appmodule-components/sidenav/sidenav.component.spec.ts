import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidenavComponent } from './sidenav.component';
import { AppComponent } from 'src/app/app.component';
import { HomeComponent } from 'src/app/modules/home/home.component';
import { LoginComponent } from 'src/app/modules/home/login/login.component';
import { NotFoundComponent } from 'src/app/modules/not-found/not-found.component';
import { ServerErrorComponent } from 'src/app/modules/server-error/server-error.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { SharedModule } from '../shared.module';
import { AppRoutingModule } from 'src/app/modules/app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from 'src/app/core/core.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('SidenavComponent', () => {
  let component: SidenavComponent;
  let fixture: ComponentFixture<SidenavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        NotFoundComponent,
        ServerErrorComponent,
        NavbarComponent,
        SidenavComponent,
      ],
      imports: [
        SharedModule,
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        CoreModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

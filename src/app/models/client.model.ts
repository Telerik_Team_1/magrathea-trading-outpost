import { Order } from './order.model';

export class Client {
  fullname: string;
  email: string;
  dateRegistered: Date;
  id: string;
  funds: number;
  allocated: number;
  profit: number;
  equity: number;
  orders: Order[];
}

import { Order } from './order.model';

export class OrderWithClient {
  order: Order;
  client: {
    fullname: string;
    id: string;
  };
}

export class Company {
  abbr: string;
  address: string;
  ceo: string;
  icon: string;
  id: string;
  name: string;
  industry: string;
  startprice: number;
  endprice: number;
  highprice: number;
  lowprice: number;
  buy: number;
  sell: number;
}


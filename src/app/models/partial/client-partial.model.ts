import { Order } from '../order.model';

export class ClientPartial {
  fullname: string;
  email: string;
  dateRegistered: Date;
  id: string;
  funds: number;
  orders: Order[];
}

import { CompanyPartial } from './company-partial.model';

export class PricePartial {
  company: CompanyPartial;
  id: string;
  highprice: number;
  lowprice: number;
  startprice: number;
  endprice: number;
  buy: number;
  sell: number;
  opendate: Date;
}

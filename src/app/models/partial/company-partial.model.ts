import { Industry } from '../industry.model';

export class CompanyPartial {
  abbr: string;
  address: string;
  ceo: string;
  icon: string;
  id: string;
  name: string;
  industry: Industry;
}

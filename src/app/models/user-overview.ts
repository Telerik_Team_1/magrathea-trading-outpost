export class UserOverviewItem {
  name: string;
  email: string;
  role: string;
}

import { ClientPartial } from './partial/client-partial.model';
import { PricePartial } from './partial/price-partial.model';
import { CompanyPartial } from './partial/company-partial.model';

export class SocketData {
  clients: ClientPartial[];
  companies: CompanyPartial[];
  prices: PricePartial[];
}

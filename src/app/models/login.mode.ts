export class LoginResponse {
  token: string;
  role: string;
}

import { CompanyPartial } from './partial/company-partial.model';

export class Order {
  id: string;
  opendate: Date;
  closedate: Date | null;
  buyprice: number;
  sellprice: number;
  profit: number;
  units: number;
  status: {
    id: string;
    statusname: string;
  };
  company: CompanyPartial;
}

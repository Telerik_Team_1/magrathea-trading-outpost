export class Message {
  constructor(
      public event: string,
      public data: string,
  ) { }
}

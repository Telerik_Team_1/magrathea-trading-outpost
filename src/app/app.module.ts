import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './modules/app-routing.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SharedModule } from './modules/shared/shared.module';
import { AuthInterceptor } from './interceptors/auth-interceptor.service';
import { ServerErrorInterceptor } from './interceptors/server-error-interceptor.service';
import { SpinnerInterceptor } from './interceptors/spinner-interceptor.service';
import { NavbarComponent } from './appmodule-components/navbar/navbar.component';
import { SidenavComponent } from './appmodule-components/sidenav/sidenav.component';
import { HomeComponent } from './appmodule-components/home/home.component';
import { LoginComponent } from './appmodule-components/home/login/login.component';
import { NotFoundComponent } from './appmodule-components/not-found/not-found.component';
import { ServerErrorComponent } from './appmodule-components/server-error/server-error.component';
import { CoreModule } from './modules/core/core.module';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    NotFoundComponent,
    ServerErrorComponent,
    NavbarComponent,
    SidenavComponent,
  ],
  imports: [
    SharedModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
